﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MyDictionary<string, int> myDictionary = new MyDictionary<string, int>();

            myDictionary.Add("One", 1);
            myDictionary.Add("Two", 2);
            myDictionary.Add("Three", 3);

            Console.WriteLine("Total count: " + myDictionary.Count);

            Console.WriteLine("Elements:");
            foreach (KeyValuePair<string, int> item in myDictionary)
            {
                Console.WriteLine(item.Key + ": " + item.Value);
            }

            Console.WriteLine("Value for key 'Two': " + myDictionary["Two"]);
        }
    }
}
