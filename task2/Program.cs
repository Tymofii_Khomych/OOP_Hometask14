﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MyList<int> myList = new MyList<int>();

            myList.Add(1);
            myList.Add(2);
            myList.Add(3);

            Console.WriteLine("Total count: " + myList.Count);

            Console.WriteLine("Elements:");
            foreach (int item in myList)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Accessing element at index 1: " + myList[1]);
        }
    }
}
